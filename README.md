D3 Examples and assignment
==========================

List
---------
* list
* of
* all
* examples

Dependencies
---------
Kartographer

External documentation
---------
http://www.simononsoftware.com/virtualenv-tutorial/
http://kartograph.org/docs/kartograph.py/

Setup and preparation
---------
* Virtualenv
    * sudo easy_install virtualenv
        * virt_env/virt1/bin/activate
        * deactivate
* Kartographer http://kartograph.org/docs/kartograph.py/install-macosx.html
    * Homebrew http://www.howtogeek.com/211541/homebrew-for-os-x-easily-installs-desktop-apps-and-terminal-utilities/
        * brew install postgresql
    * GDAL 2.1 Complete http://www.kyngchaos.com/software/frameworks
        * add export PYTHONPATH=$PYTHONPATH:/Library/Frameworks/GDAL.framework/Versions/2.1/Python/2.7/site-packages/ to .bash_profile

Issues
----------
Errors with Kartographer filters http://gis.stackexchange.com/questions/119394/how-can-i-generate-a-svg-file-of-mexico-and-usa-at-states-level-admin1-kartog
http://www.kyngchaos.com/software/qgis

